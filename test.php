<div class="output-container">
      <div class="output-sizer"> 

        <div id="result_div" class="result"><iframe id="resulte24858f43f446bcab2e0e32d0e9ec3dc1479400926586" src="https://s.codepen.io/boomerang/e24858f43f446bcab2e0e32d0e9ec3dc1479400926586/index.html" name="CodePen" allowfullscreen="true" sandbox="allow-scripts allow-pointer-lock allow-same-origin allow-popups allow-modals allow-forms" allowtransparency="true" class="result-iframe"></iframe>
          

          <div class="assets-wrap" id="assets-wrap">

  <div role="button" id="assets-area-close-button" class="assets-area-close-button">
    <svg class="icon-x">
      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#x"></use>
    </svg>
    <span class="screen-reader-text">Close</span>
  </div>


    <div class="asset-upsell">

      <div>
        <h2>Asset uploading is a <span class="badge badge-pro">PRO</span> feature.</h2>
        <p>As a PRO member, you can drag-and-drop upload files here to use as resources. Images, Libraries, JSON data... anything you want. You can even edit them anytime, like any other code on CodePen.</p>
        <p><a href="/pro/" class="button green" target="_blank">Go PRO</a></p>
      </div>

    </div>


</div>


            <section id="drawer" class="drawer">

            </section>

          

          <div id="editor-drag-cover" class="drag-cover"></div>

        </div>

        <div id="box-console" class="box box-console">

  <div class="editor-resizer editor-resizer-console" title="Drag to resize. Double-click to expand."></div>

  <div class="powers">
    <div class="powers-drag-handle" title="Drag to resize. Double-click to expand."></div>
    <div class="editor-actions-left">
      <h2 class="box-title"><span class="box-title-name">Console</span></h2>
      <div class="editor-actions-right">
        <button class="button button-medium mini-button console-clear-button" title="Clear">
          Clear
        </button>
        <button class="button button-medium mini-button close-editor-button" data-type="console" title="Close">
          <svg class="icon-x">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#x"></use>
          </svg>
        </button>
      </div>
    </div>
  </div>

  <div class="console-wrap">
    <div class="console-entries short-no-scroll"></div>
    <div class="console-command-line">
      <span class="console-arrow forwards"></span>
      <textarea class="console-command-line-input auto-expand" rows="1" data-min-rows="1"></textarea>
    </div>
  </div>

</div>


      </div> 

    </div>