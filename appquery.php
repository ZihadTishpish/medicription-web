<?php
  // define variables and set to empty values
  include_once("mobiledatabase.php");
  $db = new mobiledatabase();

if ($_SERVER["REQUEST_METHOD"] == "POST") 
{
    $query_name = $_POST["query_name"];

    if($query_name === "insertPrimaryUser")
    {
       $email = test_input ($_POST["EMAIL"]);
       $pass = test_input($_POST["PASS"]);
       $type = test_input($_POST["TYPE"]);
       $db->insertPrimaryUser($email, $pass,$type);
    }
    else if($query_name === "getDoctorId")
    {
     // echo "inside";
       $email = test_input ($_POST["EMAIL"]);
       $pass = test_input($_POST["PASS"]);
       $type = test_input($_POST["TYPE"]);
       $db->getDoctorId($email, $pass,$type);
    }

    else if($query_name === "getPatientId")
    {
     // echo "inside";
       $email = test_input ($_POST["EMAIL"]);
       $pass = test_input($_POST["PASS"]);
       $type = test_input($_POST["TYPE"]);
       $db->getPatientId($email, $pass,$type);
    }

    else if ($query_name=== "addDoctorAward")
    {
      $doc_id = test_input($_POST["DOCTOR_ID"]);
      $name = test_input($_POST["NAME"]);
      $source = test_input($_POST["SOURCE"]);
      $link = test_input($_POST["LINK"]);
      $varification = test_input('false');
      $db->addDoctorAward($doc_id, $name, $source, $link, $varification);
    }

    else if ($query_name=== "updateDoctorAward")
    {
      $doc_id = test_input($_POST["DOCTOR_ID"]);
      $name = test_input($_POST["NAME"]);
      $source = test_input($_POST["SOURCE"]);
      $link = test_input($_POST["LINK"]);
      $award_id = test_input($_POST["AWARD_ID"]);

      $db->updateDoctorAward($doc_id, $name, $source, $link, $award_id);
    }

    else if ($query_name=== "getDoctorAward")
    {
      $doc_id = test_input($_POST["DOCTOR_ID"]);
      $db->getDoctorAward($doc_id);
    }

    else if ($query_name=== "addDoctorEducation")
    {
      $DOCTOR_ID = test_input($_POST["DOCTOR_ID"]);
      $JOIN_DATE = test_input($_POST["JOIN_DATE"]);
      $LEAVE_DATE = test_input($_POST["LEAVE_DATE"]);
      $DEGREE_NAME = test_input($_POST["DEGREE_NAME"]);
      $INSTITUTE_NAME = test_input($_POST["INSTITUTE_NAME"]);
     
      $db->addDoctorEducation($DOCTOR_ID, $JOIN_DATE, $LEAVE_DATE, $DEGREE_NAME, $INSTITUTE_NAME);
    }

    else if ($query_name=== "updateDoctorEducation")
    {
      $INSTITUTE_ID = test_input($_POST["INSTITUTE_ID"]);
      $DOCTOR_ID = test_input($_POST["DOCTOR_ID"]);
      $JOIN_DATE = test_input($_POST["JOIN_DATE"]);
      $LEAVE_DATE = test_input($_POST["LEAVE_DATE"]);
      $DEGREE_NAME = test_input($_POST["DEGREE_NAME"]);
      $INSTITUTE_NAME = test_input($_POST["INSTITUTE_NAME"]);
     
      $db->updateDoctorEducation($DOCTOR_ID, $JOIN_DATE, $LEAVE_DATE, $DEGREE_NAME, $INSTITUTE_NAME, $INSTITUTE_ID);
    }

     else if ($query_name=== "getDoctorEducation")
    {
      $doc_id = test_input($_POST["DOCTOR_ID"]);
      $db->getDoctorEducation($doc_id);
    }
	
	else if ($query_name=== "getDoctorCategory")
    {
      $doc_id = test_input($_POST["DOCTOR_ID"]);
      $db->getDoctorCategory($doc_id);
    }



	
	else if ($query_name=== "getPatientAge")
    {
      $patient_id = test_input($_POST["PATIENT_ID"]);
      $db->getPatientAge($patient_id);
    }
	
	else if ($query_name=== "getPatientGender")
    {
      $patient_id = test_input($_POST["PATIENT_ID"]);
      $db->getPatientGender($patient_id);
    }


    else if($query_name=== "getHospitalId")
    {
      //echo "inside";
       $name = test_input ($_POST["NAME"]);
       $location = test_input($_POST["LOCATION"]);
       $district = test_input($_POST["DISTRICT"]);

       $db->getHospitalId($name, $location,$district);
    }


    else if ($query_name=== "addDoctorHospital")
    {
      $HOSPITAL_ID = test_input($_POST["HOSPITAL_ID"]);
      $DOCTOR_ID = test_input($_POST["DOCTOR_ID"]);
      $JOIN_DATE = test_input($_POST["JOIN_DATE"]);
      $LEAVE_DATE = test_input($_POST["LEAVE_DATE"]);
      $FEE = test_input($_POST["FEE"]);
      $SCHEDULE = test_input($_POST["SCHEDULE"]);
     
      $db->addDoctorHospital($DOCTOR_ID, $HOSPITAL_ID,$JOIN_DATE,$LEAVE_DATE,$FEE,$SCHEDULE);
    }

    else if ($query_name=== "updateDoctorHospital")
    {
      $HOSPITAL_ID = test_input($_POST["HOSPITAL_ID"]);
      $DOCTOR_ID = test_input($_POST["DOCTOR_ID"]);
      $JOIN_DATE = test_input($_POST["JOIN_DATE"]);
      $LEAVE_DATE = test_input($_POST["LEAVE_DATE"]);
      $FEE = test_input($_POST["FEE"]);
      $SCHEDULE = test_input($_POST["SCHEDULE"]);
     
      $db->updateDoctorHospital($DOCTOR_ID, $HOSPITAL_ID,$JOIN_DATE,$LEAVE_DATE,$FEE,$SCHEDULE);
    }

    else if ($query_name=== "getDoctorHospital")
    {
      $doc_id = test_input($_POST["DOCTOR_ID"]);
      $db->getDoctorHospital($doc_id);
    }

    else if ($query_name=== "addDoctorPublication")
    {
      $doc_id = test_input($_POST["DOCTOR_ID"]);
      $name = test_input($_POST["NAME"]);
      $dates = test_input($_POST["DATES"]);
      $link = test_input($_POST["LINK"]);
      $varification = test_input('false');
      $db->addDoctorPublication($doc_id, $name, $dates, $link, $varification);
    }
    
    else if ($query_name=== "updateDoctorPublication")
    {
      $doc_id = test_input($_POST["DOCTOR_ID"]);
      $name = test_input($_POST["NAME"]);
      $dates = test_input($_POST["DATES"]);
      $link = test_input($_POST["LINK"]);
      $publication_id=test_input($_POST["PUBLICATION_ID"]);
      $db->updateDoctorPublication($doc_id, $name, $dates, $link, $publication_id);
    }

     else if ($query_name=== "getDoctorPublication")
    {
      $doc_id = test_input($_POST["DOCTOR_ID"]);
      $db->getDoctorPublication($doc_id);
    }


    else if ($query_name=== "getHospitalName")
    {
        $hos_id = test_input($_POST["HOSPITAL_ID"]);
        $db->getHospitalName($hos_id);
    }





    else if ($query_name=== "addPatient")
    {
      $USER_ID = test_input($_POST["USER_ID"]);
      $NID = test_input($_POST["NID"]);
      $GENDER = test_input($_POST["GENDER"]);
      $FIRST_NAME = test_input($_POST["FIRST_NAME"]);
      $LAST_NAME = test_input($_POST["LAST_NAME"]);
      $DOB = test_input($_POST["DOB"]);
      $PRESENT_ADDRESS = test_input($_POST["PRESENT_ADDRESS"]);

      $db->addPatient( $USER_ID, $NID, $GENDER, $FIRST_NAME, $LAST_NAME, $DOB, $PRESENT_ADDRESS);
    }

    else if ($query_name=== "updatePatient")
    {
      $PATIENT_ID = test_input($_POST["PATIENT_ID"]);
      $NID = test_input($_POST["NID"]);
      $GENDER = test_input($_POST["GENDER"]);
      $FIRST_NAME = test_input($_POST["FIRST_NAME"]);
      $LAST_NAME = test_input($_POST["LAST_NAME"]);
      $DOB = test_input($_POST["DOB"]);
      $PRESENT_ADDRESS = test_input($_POST["PRESENT_ADDRESS"]);

      $db->updatePatient( $PATIENT_ID, $NID, $GENDER, $FIRST_NAME, $LAST_NAME, $DOB, $PRESENT_ADDRESS);
    }


    else if ($query_name=== "addMedicine")
    {
        $TYPE= test_input($_POST["TYPE"]);
        $NAME= test_input($_POST["NAME"]);
        $STRENGTH= test_input($_POST["STRENGTH"]);
        $MANUFACTURER= test_input($_POST["MANUFACTURER"]);
        $PRICE= test_input($_POST["PRICE"]);
        $db->addMedicine( $TYPE, $NAME,$STRENGTH,$MANUFACTURER,$PRICE);
    }

    else if ($query_name=== "getMedicineId")
    {
        //"post received";
        $TYPE= test_input($_POST["TYPE"]);
        $NAME= test_input($_POST["NAME"]);
        $STRENGTH= test_input($_POST["STRENGTH"]);
        $MANUFACTURER= test_input($_POST["MANUFACTURER"]);
        $PRICE= test_input($_POST["PRICE"]);
        $db->getMedicineId($TYPE, $NAME,$STRENGTH,$MANUFACTURER,$PRICE);
    }

    else if ($query_name=== "getAllMedicine")
    {
        $db->getAllMedicine();
    }

    else if ($query_name=== "getOperationId")
    {
        $TYPE= test_input($_POST["OPERATION_TYPE"]);
        $NAME= test_input($_POST["OPERATION_NAME"]);
        $db->getOperationId($NAME,$TYPE);
    }

    else if ($query_name=== "getAllOperation")
    {
        $db->getAllOperation();
    }

     else if ($query_name=== "getAllPrescription")
    {
        $DOCTOR_ID = test_input($_POST["DOCTOR_ID"]);
        $db->getAllPrescription($DOCTOR_ID);
    }

    else if ($query_name=== "getTestId")
    {
        $TYPE= test_input($_POST["TEST_TYPE"]);
        $NAME= test_input($_POST["TEST_NAME"]);
        $db->getTestId($NAME,$TYPE);
    }

     else if ($query_name=== "getAllTest")
    {
        $db->getAllTest();
    }

	 else if ($query_name=== "addPrescriptionRequest")
    {

  	$DOCTOR_ID= test_input($_POST["DOCTOR_ID"]);
  	$PATIENT_ID= test_input($_POST["PATIENT_ID"]);
  	$DATES= test_input($_POST["DATES"]);
  	$TIMES= test_input($_POST["TIMES"]);
  	$PRIVACY= "OPEN";
  	$STATUS = "REQUESTED";
    $db->addPrescriptionRequest($DOCTOR_ID, $PATIENT_ID,$DATES,$TIMES,$PRIVACY,$STATUS);
    }

	

	else if ($query_name=== "updatePrescriptionFromDoctor")
  {

	$DOCTOR_ID= test_input($_POST["DOCTOR_ID"]);
	$PATIENT_ID= test_input($_POST["PATIENT_ID"]);
	$DATES= test_input($_POST["DATES"]);
	$TIMES= test_input($_POST["TIMES"]);
	$PRIVACY= test_input($_POST["PRIVACY"]);
	$STATUS = test_input($_POST["STATUS"]);
	$PRESCRIPTION_ID= test_input($_POST["PRESCRIPTION_ID"]);
	$PATIENT_BP = test_input($_POST["PATIENT_BP"]);
	$SUMMARY=test_input($_POST["SUMMARY"]);
  $db->updatePrescriptionFromDoctor($DOCTOR_ID, $PATIENT_ID,$DATES,$TIMES,$PRIVACY,$STATUS, $PRESCRIPTION_ID,$PATIENT_BP,$SUMMARY);
  
  }

	else if ($query_name=== "updatePrescriptionFromPatient")
  {

  $PRIVACY= test_input($_POST["PRIVACY"]);
  $PRESCRIPTION_ID= test_input($_POST["PRESCRIPTION_ID"]);
  $db->updatePrescriptionFromPatient($PRIVACY ,$PRESCRIPTION_ID);

  }

	
	else if ($query_name=== "addPrescriptionMedicine")
  {
	
	$MEDICINE_ID= test_input($_POST["MEDICINE_ID"]);
	$PRESCRIPTION_ID= test_input($_POST["PRESCRIPTION_ID"]);
	$SCHEDULE= test_input($_POST["SCHEDULE"]);
	$DURATION= test_input($_POST["DURATION"]);
	$COMMENTS= test_input($_POST["COMMENTS"]);

  $db->addPrescriptionMedicine($MEDICINE_ID, $PRESCRIPTION_ID, $SCHEDULE, $DURATION, $COMMENTS);
  }


  else if ($query_name=== "getAllDoctor")
  {
    $db->getAllDoctor();
  }

	else if ($query_name=== "addPrescriptionOperation")
  {

	
	$OPERATION_ID= test_input($_POST["OPERATION_ID"]);
	$PRESCRIPTION_ID= test_input($_POST["PRESCRIPTION_ID"]);
	$TIME= test_input($_POST["TIME"]);
	$COMMENTS= test_input($_POST["COMMENTS"]);

  $db->addPrescriptionOperation($OPERATION_ID, $PRESCRIPTION_ID, $TIME,  $COMMENTS);
  }


	else if ($query_name=== "addPrescriptionTest")
  {
	$TEST_ID= test_input($_POST["TEST_ID"]);
	$PRESCRIPTION_ID= test_input($_POST["PRESCRIPTION_ID"]);
	$REQUIREMENTS= test_input($_POST["REQUIREMENTS"]);
	$COMMENTS= test_input($_POST["COMMENTS"]);
  $db->addPrescriptionTest($TEST_ID, $PRESCRIPTION_ID, $REQUIREMENTS, $COMMENTS);
  }

   else if ($query_name=== "getPrescriptionMedicine")
  {
    $PRESCRIPTION_ID= test_input($_POST["PRESCRIPTION_ID"]);
    $db->getPrescriptionMedicine($PRESCRIPTION_ID);

  }

   else if ($query_name=== "getPrescriptionOperation")
  {
    $PRESCRIPTION_ID= test_input($_POST["PRESCRIPTION_ID"]);
    $db->getPrescriptionOperation($PRESCRIPTION_ID);

  }
  else if ($query_name=== "getPrescriptionTest")
  {
    $PRESCRIPTION_ID= test_input($_POST["PRESCRIPTION_ID"]);
    $db->getPrescriptionTest($PRESCRIPTION_ID);

  }

  else if ($query_name=== "addRating")
  {
    $DOCTOR_ID= test_input($_POST["DOCTOR_ID"]);
    $PATIENT_ID= test_input($_POST["PATIENT_ID"]);
    $RATING= test_input($_POST["RATING"]);
    $DATE= test_input($_POST["DATE"]);
    $COMMENT= test_input($_POST["COMMENT"]);

    $db->addRating($DOCTOR_ID, $PATIENT_ID, $RATING, $DATE, $COMMENT);

  }

  else if ($query_name=== "updateRating")
  {
    $DOCTOR_ID= test_input($_POST["DOCTOR_ID"]);
    $PATIENT_ID= test_input($_POST["PATIENT_ID"]);
    $RATING= test_input($_POST["RATING"]);
    $DATE= test_input($_POST["DATE"]);
    $COMMENT= test_input($_POST["COMMENT"]);
    $RATING_ID=test_input($_POST["RATING_ID"]);
    $db->updateRating($DOCTOR_ID, $PATIENT_ID, $RATING, $DATE, $COMMENT,$RATING_ID);

  }


  else if ($query_name==="getAllRating")
  {
    $DOCTOR_ID= test_input($_POST["DOCTOR_ID"]);
    $db->getAllRating($DOCTOR_ID);
  }

  else if ($query_name==="getPatientInfo")
  {
    $DOCTOR_ID= test_input($_POST["PATIENT_ID"]);
    $db->getPatientInfo($DOCTOR_ID);
  }

  else if ($query_name==="getAvgRating")
  {
    //echo "Here in Rating Average giving Error";
    $DOCTOR_ID= test_input($_POST["DOCTOR_ID"]);
    $db->getRatingAvg($DOCTOR_ID);
  }

  else if ($query_name==="getDoctorName")
  {
    //echo "hiiiiiiiiiii";
    $DOCTOR_ID= test_input($_POST["DOCTOR_ID"]);
    $db->getDoctorName($DOCTOR_ID);
  }


  //in development

  else if ($query_name==="getPatientName")
  {
    //echo "hiiiiiiiiiii";
    $PATIENT_ID= test_input($_POST["PATIENT_ID"]);
    $db->getPatientName($PATIENT_ID);
  

  }

  //in development



  else if ($query_name==="getPatientCount")
  {
    //echo "hiiiiiiiiiii";
    $DOCTOR_ID= test_input($_POST["DOCTOR_ID"]);
    $db->getPatientCount($DOCTOR_ID);
  }

   else if ($query_name==="addDoctorFirebase")
  {
    //echo "hiiiiiiiiiii";
    $DOCTOR_ID= test_input($_POST["DOCTOR_ID"]);
    $FIREBASE= test_input($_POST["FIREBASE"]);
    $db->addDoctorFirebase($DOCTOR_ID,$FIREBASE);
  }

else if ($query_name==="getDoctorFirebase")
  {
    //echo "hiiiiiiiiiii";
    $DOCTOR_ID= test_input($_POST["DOCTOR_ID"]);
    $db->getDoctorFirebase($DOCTOR_ID);
  }

  else if ($query_name==="getPatientFirebase")
  {
    //echo "hiiiiiiiiiii";
    $PATIENT_ID= test_input($_POST["PATIENT_ID"]);
    $db->getPatientFirebase($PATIENT_ID);
  }

  else if ($query_name==="addPatientFirebase")
  {
    //echo "hiiiiiiiiiii";
    $PATIENT_ID= test_input($_POST["PATIENT_ID"]);
    $FIREBASE= test_input($_POST["FIREBASE"]);
    $db->addPatientFirebase($PATIENT_ID,$FIREBASE);
  }

  else if ($query_name=== "getAllRequest")
  {
      /*echo "dekhi";
      $DOCTOR_ID= test_input($_POST["DOCTOR_ID"]);
       echo $DOCTOR_ID;
      $db->getPrescriptionRequest($DOCTOR_ID);
      echo "hihihihihihh";*/
      $DOCTOR_ID= test_input($_POST["DOCTOR_ID"]);
      $db->getAllRequest($DOCTOR_ID);
  }


  else
  {
    echo "no match";
  }

}
else
{
  echo "error";
}


function test_input($data) 
{
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

?>